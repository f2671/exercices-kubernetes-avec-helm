# Exercices Kubernetes avec Helm

Ces exercices sont liés au TP3 de la session d'ateliers sur Docker, Kubernetes et Helm.

L'enjeu est ici de pratiquer et de s'initier au travail sur cluster Kubernetes. Nous évoquerons certains concepts spécifiques et avons comme objet un fil rouge précisé lors de la premiere et seconde seance : 
Héberger une application - Tomcat / BDD Postgresql packagée sur un cluster Kubernetes.

Lien vers les slides : https://f2671.gitlab.io/formation-docker-kube-support/formation-docker-kube-2022#/4

## Prérequis d'installation 

### Prerequis Kubernetes
- Installation du client pour l'api kubernetes **kubectl** (windows) :
  - Télécharger la dernière version du client ici : https://dl.k8s.io/release/v1.23.0/bin/windows/amd64/kubectl.exe
  - Déplacer ce .exe téléchargé dans un endroit convenable (e.g. ~/programmes ou dans C:/INSEE par ex)
  - Ajouter à la variable d'environnement PATH une entrée qui pointe vers le répertoire qui contient le .exe (permet a windows de le retrouver)
- Création d'un compte SSPCLOUD : https://dev.insee.io/ ou https://datalab.sspcloud.fr/ (si ce n'est pas déjà fait et si vous souhaitez travailler sur un des 2 clusters de la DIIT)

### Prerequis Helm
- Installation du client Helm en prenant l'une des release ici : https://github.com/helm/helm/releases

> La configuration de Helm, notamment en ce qui concerne l'authentification se base sur la même config que kubectl. 

### Validation  

Validez votre installation en tapant depuis une invite de commande après avoir rafraichî vos variables d'environnement : 
```
kubectl version
```

```
helm version
```



## Que retenir de la séance

### Architecture Kubernetes

<img src="https://csharpcorner-mindcrackerinc.netdna-ssl.com/article/getting-started-with-kubernetes-part2/Images/1.png"/>

> Une API auquel on envoie des fichiers, cela implique la création d'applications sur un cluster de machines.

### Helm

<img src="https://bothe.at/wp-content/uploads/2021/01/image-39-1024x445.png">

> Permet de packager la partie YAML dans un livrable "CHART" pour une installation simplifiée

## 0 - Rappel - Accéder à un cluster Kubernetes : Authentification

Cette partie précise un élément pourtant important avant de travailler sur un cluster kubernetes, l'authentification.

Elle propose des pistes pour l'authentification sur les clusters INSEE actuels. 

- [Accéder à la sous partie ▶️](sous-parties/authentification/)


## Prerequis avant Helm : Rappels sur la configuration du fil rouge

La configuration de l'application effectuée la semaine dernière a nécessité la créations de multiples fichiers.

Il y a également certains cas de reproduction et de références que l'on aimerait pouvoir configurer.

- [Accéder à la sous partie ▶️](sous-parties/prerequis-helm/)


## 1 - Utilisation d'une application packagée et préconfigurée
Dans la vie de tous les jours, vous n'êtes pas les seuls a vouloir héberger des tomcat ou des bases de données sur des clusters Kubernetes.

Pour cela, une idée a été de construire un méta-package de fichiers de configuration YAML pour Kubernetes : on parle alors de chart Helm.

> Ce n'est pas la seule technologie qui va dans le sens de la mutualisation / templating ([Operator](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/) par exemple) 

- [Accéder a la sous partie ▶️](sous-parties/helm-utilisation/)


## 2 - Packaging d'une application a partir de ses fichiers YAML : Helm

Par la même, vous pouvez être amenés a vouloir packager votre application pour la rendre facilement installable sur plusieurs clusters, et cela avec un seul fichier zippé.

> Dans cette partie, nous repartons de fichiers de configuration d'un tomcat et de sa base de données PostgreSQL

> La construction de ses fichiers est détaillée ici : https://gitlab.com/f2671/exercices-kubernetes

- [Accéder a la sous partie ▶️](sous-parties/fil-rouge/)
## 3 - Installation sur plusieurs clusters

Maintenant il faut sauvegarder vos fichiers de config dans un dépot dédié.

Helm apporte un concept qui lui est propre, les helm repository.

Une fois accessibles, il est assez simple d'installer un paquet sur un cluster.

- [Accéder a la sous partie ▶️](sous-parties/installation/)


**Pour les parties suivantes, il vous sera demandé de créer un projet sur une instance de gitlab interne a votre réseau**

## 4 - Job : Cas des batchs


Nous allons étudier un composant Kubernetes : le Job. 
Le cas d'usage d'un job est celui d'un batch.

Vous pouvez en effet prévoir une fréquence ou non d'execution, et demander l'execution d'un batch via API Kubernetes. 

- [Accéder à la sous partie ▶️](sous-parties/batchs-et-jobs/)



