# Sauvegarde et Installation depuis un cluster

L'objectif de cette partie est double : 

- Faire vivre une application qui a été lancée et packagée par helm
- Créer un paquet et l'héberger

Deux commandes essentiellement dans cette partie :

## Création d'un package

```
helm package <chemin-vers-le-dossier-chart>
```

Cette commande crée un fichier compressé tar.gz qui contient toute la configuration.

Et de la meme façon on peut installer un chart depuis un tar.gz :

```
helm install initiation-docker-kubernetes-0.1.0.tgz --generate-name
```

## Mise a jour d'un environnement

```
helm upgrade -f myvalues.yaml -f override.yaml <nom-chart> <nom-instance-chart>
```

C'est typiquement ce que l'on peut être amenés a faire pour mettre a jour une version dans un environnement de type dev/recette.





## [Retour a la page principale ↩️ ](../../README.md)