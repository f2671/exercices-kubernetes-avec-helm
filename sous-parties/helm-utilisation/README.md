# Utilisation de helm : installation de paquets packagés


Helm est une solution permettant la mutualisation des configurations standard au travers de paquets que l'on appelle charts.

Typiquement, dans un cas classique, lorsqu'il s'agit d'installer une application connue, on va préférer aux fichiers de configuration brutes, la configuration dans un fichier unique d'une application. Cela permet de s'abstraire du parcours dans les services et de plutôt se fier a une documentation d'utilisation spécifique au service.

## Fonctionnement de Helm

Le fonctionnement de Helm repose sur le templating [Jinja](https://jinja.palletsprojects.com/en/3.0.x/).

Depuis un dossier, lancez la commande qui permet de créer un chart par défaut : 

```
helm create mychart
```

> Vous n'êtes pas obligés, on passera plus de temps a la construction d'un chart dans une partie suivante

<img src="https://razorops.com/images/blog/helm-3-tree.png"/>

1. Chart.yaml

Le principe est de définir un fichier qui définit le paquet : le fichier Chart.yaml.

Ce fichier décrit les métadonnées du paquet.

2. templates/*

On regroupe tous nos fichiers yaml nécessaire pour l'application dans le dossier template. Il seront appliqués lors de l'installation via Helm.

Si vous ouvrez un des fichiers dans ce dossier, par exemple le fichier deployment.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "chart.fullname" . }}
  labels:
    {{- include "chart.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "chart.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "chart.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "chart.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
```

> deployment.yaml

Le fichier ressemble a un fichier que l'on a pu construire sur le TP2, mais des valeurs encadrées par des chevrons {{}} encadrent certaines valeurs.

Ce sont ces valeurs qui vont pouvoir être valorisées de notre côté.

3. values.yaml

La valorisation est centralisée dans un fichier : le fichier values.yaml

Ainsi la définition des image dans le values.yaml : 

```yaml
image:
  repository: nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""
```
> values.yaml

Se traduit dans l'usage :
```yaml
containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
```

> templates/deployment.yaml

Les chevrons {{ .Values.image.repository }} se font valoriser par image.repository dans le fichier values.yaml
## Exemple avec le helm chart PostgreSQL

Parcourons ensemble le dépot du chart:

https://github.com/bitnami/charts/tree/master/bitnami/postgresql

## Utilisation d'un chart

Pour utiliser un chart, il faut d'abord le trouver. De nombreux dépôts existent et vous devez les ajouter via la commande : 

```
helm repo add <repo> url
```

exemple :

```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

ou le depot inseefrlab utilisé par Onyxia par exemple :

```
helm repo add inseefrlab-datascience https://inseefrlab.github.io/helm-charts-datascience
```

Vous êtes invités a mettre a jour les dépots pour avoir toutes les informations a jour en local : 

```
helm repo update
```

## Utilisation d'un chart

Helm propose une commande qui permet d'installer des paquets en une ligne :

```
helm install <chart-helm>
```

Essayez par exemple : 

```
helm install mon-postgres bitnami/postgresql
```

Vous pouvez observer son installation via kubectl par ex : 
```
kubectl get all
```

## Configuration

Vous pouvez toujours configurer un chart. Cela revient a surcharger les valeurs présentes dans le fichier values.yaml

ex : https://github.com/bitnami/charts/blob/master/bitnami/postgresql/values.yaml 

Le README détaille également les variables que l'on peut configurer directement dans le fichier values.yaml

> Un avantage est que du coup on peut simplement configurer les quelques lignes que l'on souhaite préciser, et laisser le reste par défaut

Par exemple si l'on veut créer un user postgres sur la base de données : la documentation nous invite a configurer auth.postgresPassword du values.yaml

https://github.com/bitnami/charts/blob/master/bitnami/postgresql/values.yaml#L4-L29

Cela donne, avec les fichiers exemple : 

```
helm install mon-postgres bitnami/postgresql -f ./sous-parties/helm-utilisation/assets/values-example.yaml
```

Vous pouvez également mettre a jour une version qui tourne avec la commande upgrade : 

```
helm upgrade -f ./sous-parties/helm-utilisation/assets/values-example.yaml mon-postgres
```

> Pour information, les mot de passe sont stockées dans le volume du postgres, il faut donc le tuer pour que les mots de passes soient bien mis a jour

```
kubectl delete pvc data-mon-postgres-postgresql-0
```

Vérifications : 
```
kubectl port-forward svc/mon-postgres-postgresql 5432:5432
```

> Je met le postgres hébergé sur le cluster kubernetes sur mon port 5432

```
psql -U postgres
```
> Je vérifie avec mon psql en local que j'accède bien a la bdd avec le mot de passe du values-example.yaml : mdp

## [Retour a la page principale ↩️ ](../../README.md)