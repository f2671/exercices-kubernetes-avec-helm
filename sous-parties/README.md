# Sous parties

Le support a été séparé en sous parties pour plusieurs raisons : 

- Permettre de clarifier et de ne pas surcharger un fichier README
- Favoriser une navigation 
- Permettre de donner les fichiers de configuration pour la sous partie, ce qui permet donc d'executer de son côté différentes commandes.

> Remarque : Les fichier seront stockés dans /sous-partie/$NOM_SOUS_PARTIE/assets/

## Sommaire

-  [authentification](/sous-parties/authentification/README.md)
-  [retour sur les fichiers de la session precedente](/sous-parties/prerequis-helm/README.md)
-  [installation d'une application packagée](/sous-parties/helm-utilisation/README.md)
-  [packaging d'une application tomcat/postgres](/sous-parties/exterieur/README.md)
-  [sauvegarde du paquet et installation dans différents clusters](/sous-parties/installation/README.md)
-  [les batchs](/sous-parties/batchs-et-jobs/README.md)