# Packaging de l'application fil rouge avec HELM

Dans cette partie on va s'intéresser a packager notre application Tomcat + Postgresql avec Helm.

> Nous avons importés les fichiers du TP précédent dans ./sous-parties/assets/

> Avec postgres/ pour les fichiers de configuration de notre base postgres et tomcat/ pour les fichiers de configuration de notre tomcat

Repartons d'un chart exemple : 

```
helm create mychart
```

## Structure générale

### Fichier Chart.yaml

```yaml
apiVersion: v2
name: mychart
description: A Helm chart for Kubernetes

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 0.1.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
# It is recommended to use it with quotes.
appVersion: "1.16.0"
```

> Chart.yaml

Ce fichier recense les dépendances du projet avec d'autres charts, et les métadonnées du chart.

TP : Vous pouvez par exemple remplacer les valeurs des clés **name** **description** **version**

### Dossier templates/

C'est le dossier qui contient tous les yaml qui seront variabilisés pour votre application. Supprimez donc les 
Il s'agit ici de variabiliser les yaml et de les inserer dans le dossier templates.

Ce que l'on veut variabiliser :

- Les variables d'environnement
- Les noms de service et d'app (quitte a ne les surcharger que dans des cas précis)
- Les ports des services (contraintes machines par ex)

### Fichier values.yaml

Fichier qui centralise tous les éléments configurables des fichiers du chart.

```yaml
image:
  repository: nginx
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: ""
```

> values.yaml

On peut choisir de surcharger ou non les valeurs des variables renseignées pour une installation sur un environnement cible.

### TP : Variabiliser les fichiers, travailler sur des YAML

Récupérez les fichiers existants dans assets/ et variabilisez les.

> hint une solution est dans la partie installation
## [Retour a la page principale ↩️ ](../../README.md)
