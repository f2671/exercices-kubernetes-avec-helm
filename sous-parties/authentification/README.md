# Authentification 

**REMARQUE: Cette partie est la même que le tp précédent, en effet helm se sert de la config de kubectl**

Cette partie est spécifique à l'implémentation. Dans le cadre du travail sur des clusters INSEE, on préferera donc parler d'authentification avec jeton oidc.

> Remarque : elle est pourtant importante car abordée dans tout les contextes (GCP/AWS/Interne/Externe..)

> Note: Si vous êtes sur votre poste perso, ou que vous avez des droits élevés sur votre poste, vous pouvez essayer minikube, cela lance un cluster avec un noeud sur votre machine, de quoi tester en local vos déploiements.

Clusters disponibles : 

- Le cluster de l'innovation GKE : dev.insee.io => Accessible via authentification oidc externe
- Le cluster interne : kube.developpement.insee.fr => Accessible via authentification oidc externe mais sur réseau interne
- Le cluster de l'innovation du Datalab : https://datalab.sspcloud.fr/ => Accessible seulement depuis le réseau interne au cluster et donc depuis les vscode que vous pouvez créer à la demande.



## Authentification dans le cas des 2 premiers clusters 

<p align="center"><kbd>
<img src="https://www.magalix.com/hs-fs/hubfs/Authentication%203-1.png?width=720&name=Authentication%203-1.png" />
</kbd>
</p>


C'est une authentification OIDC : 
    - Vous allez devoir récupérer un token JWT
    - le rajouter dans votre profil dans le client kubectl en tant que token

Comment faire : 

Il vous faut donc dire à quel cluster vous souhaitez accéder : `kubectl config set-cluster $NOM_CLUSTER $URL`

(doc https://jamesdefabia.github.io/docs/user-guide/kubectl/kubectl_config_set-cluster/)

Et s'authentifier via jeton avec l'option --token de : `kubectl config set-credentials $NOM_CREDENTIALS [options]`

(doc https://jamesdefabia.github.io/docs/user-guide/kubectl/kubectl_config_set-credentials/)

> Remarque vous pouvez définir plusieurs config pour plusieurs clusters : `context`
## Application avec les clusters a disposition

> Pour le cluster https://datalab.sspcloud.fr/ tout est déjà préconfiguré

**Remarque : En fait, une application d'onboarding permet de vous créer ici un espace où vous avez de nombreux droits, et où vous êtes le seul (avec les admins) autorisé.**

- Pour le cluster GKE de l'innovation : https://dev.insee.io/
- Pour le cluster Interne INSEE (lab) : https://welcome.kube.developpement.insee.fr


- Après authentification accédez a l'onglet : 'Créer mon espace de travail (namespace)'

Par défaut, les droits vous sont octroyés sur des **namespace** propres, crées par cette même application d'onboarding.

- Une fois votre espace personnel crée, vous pouvez récupérer un script init.sh.

Il comprend tout le nécessaire pour s'authentifier : nom du cluster, token généré a la volée et également vous place dans votre espace de travail


## [Retour a la page principale ↩️ ](../../README.md)