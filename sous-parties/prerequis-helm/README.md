# Retour sur les fichiers du TP précédent

Ici certaines libertés de vulgarisation sont prises, mais l'objectif est de faire un état des lieux global pour avancer.

## Postgres 
On voulait héberger une Base de Données qui demande un disque persistant.

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres-app
spec:
  selector:
    matchLabels:
      app: postgres-app
  serviceName: postgres-app-service
  replicas: 1
  template:
    metadata:
      labels:
        app: postgres-app
    spec:
      containers:
      - name: postgres-app
        image: ragatzino/initiation-docker-kube-postgres:latest
        ports:
        - containerPort: 5432
        volumeMounts:
        - name: disque-postgres
          mountPath: /data
        env:
          # rapport au montage du disque sur /data
          - name: PGDATA
            value: /data/pgdata
         # - name: POSTGRES_USER
         #   value: postgres
         # - name: POSTGRES_PASSWORD
         #   value: mdp
         # - name: POSTGRES_DB
         #   value: postgres
        envFrom:
          - configMapRef:
              name: postgresapp-configmap
          - secretRef:
              name: postgresapp-secret

  volumeClaimTemplates:
  - metadata:
      name: disque-postgres
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

> postgres-app-statefulset.yaml

## Détail 


### Image et exposition
On peut découper le fichier en 3 parties : 

> Je désire une base postgres, pour le coup celle de mon appli que j'ai déjà packagée avec des données de test en tant que "ragatzino/initiation-docker-kube-postgres" et la rendre disponible sur le port 5432 du pod

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres-app
spec:
  selector:
    matchLabels:
      app: postgres-app
  serviceName: postgres-app-service
  replicas: 1
  template:
    metadata:
      labels:
        app: postgres-app
    spec:
      containers:
      - name: postgres-app
        image: ragatzino/initiation-docker-kube-postgres:latest
        ports:
        - containerPort: 5432
```

> Je desire aussi que ce port 5432 du pod il soit accessible a une adresse fixe dans le cluster : postgres-app-service:5432

```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres-app-service
spec:
  selector:
    app: postgres-app
  ports:
  - port: 5432
    targetPort: 5432
```

> postgres-app-service.yaml

### Environnement

> Je désire configurer quelques variables d'environnement et mot de passe 

```yaml
        env:
          # rapport au montage du disque sur /data
          - name: PGDATA
            value: /data/pgdata
         # - name: POSTGRES_USER
         #   value: postgres
         # - name: POSTGRES_PASSWORD
         #   value: mdp
         # - name: POSTGRES_DB
         #   value: postgres
        envFrom:
          - configMapRef:
              name: postgresapp-configmap
          - secretRef:
              name: postgresapp-secret
```
> postgres-app-statefulset.yaml

Avec les configmap et secrets associes (fichiers de configuration environnement clé/valeur): 

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: postgresapp-secret
type: Opaque
stringData:
  POSTGRES_PASSWORD: mdp
```

> postgres-app-secret.yaml

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgresapp-configmap
data:
  POSTGRES_USER: postgres
  POSTGRES_DB: postgres
```

> postgres-app-configmap.yaml

### Persistence

> Je désire persister tout ça et demander a Kubernetes de me donner un espace dédié d'un Go sur /data pour chaque instance

```yaml
        volumeMounts:
        - name: disque-postgres
          mountPath: /data
```

> postgres-app-statefulset.yaml


et 

```yaml
  volumeClaimTemplates:
  - metadata:
      name: disque-postgres
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

> postgres-app-statefulset.yaml

## Tomcat

Et pour le tomcat, on voulait un tomcat accessible sur une URL publique, qui se connecte a la BDD pour notre application : **ragatzino/initiation-docker-kube-tomcat:latest**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tomcatapp-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: tomcatapp-deployment
  template:
    metadata:
      labels:
        app: tomcatapp-deployment
    spec:
      containers:
        - name: tomcatapp-container
          image: ragatzino/initiation-docker-kube-tomcat:latest
          ports:
            - containerPort: 8080
          envFrom:
          - configMapRef:
              name: tomcatapp-configmap
          env:
          - name: SPRING_DATASOURCE_USERNAME
            valueFrom:
              configMapKeyRef:
                name: postgresapp-configmap
                key: POSTGRES_USER
          - name: SPRING_DATASOURCE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: postgresapp-secret
                key: POSTGRES_PASSWORD
```

> tomcatapp-deployment.yaml

En récuperant la configuration pour spécifier qu'on est sur une bdd postgres et s'y connecter : 

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: tomcatapp-configmap
data:
  HELLO_MESSAGE: bonjour
  SPRING_H2_CONSOLE_ENABLED: "false"
  SPRING_DATASOURCE_URL: "jdbc:postgresql://postgres-app-service:5432/postgres"
  SPRING_DATASOURCE_DRIVERCLASSNAME: "org.postgresql.Driver"
  SPRING_JPA_DATABASE-PLATFORM: "org.hibernate.dialect.PostgreSQLDialect"
```

Et en exposant en interne puis a l'exterieur du cluster:

En interne :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: tomcatapp-service
spec:
  type: LoadBalancer
  selector:
# metadata labels du deployment par ex
    app: tomcatapp-deployment
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 8080
```

> tomcatapp-service.yaml

En externe :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: tomcatapp-ingress
  labels:
    name: tomcatapp-ingress
spec:
  rules:
  - host: ateliersndiokub.dev.insee.io
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
          ## metadata du service
            name: tomcatapp-service
            port: 
              number: 8080
```
> tomcatapp-ingress.yaml

Accessible sur https://ateliersndiokub.dev.insee.io

## Quels problèmes ?

- On est obligé de vous déballer les fichiers un a un pour expliquer les éléments de configuration alors qu'il y a une grande partie de ses fichiers qui sont standard, et donc qui finalement polluent la vue pour une installation.
- Au final peu de choses changeraient d'une installation a une autre, se serait plus confortable de centraliser toutes ces différences dans un fichier.
- On est pas les seuls au monde a installer des postgres, se serait bien qu'on trouve une installation de quelqu'un auquel on apporte plus de crédit, qui a des followers sur StackOverflow par exemple.

C'est en partie pour cela que les gens se sont mis a partager des fichiers de configuration packagés, on va donc pouvoir passer a helm.

