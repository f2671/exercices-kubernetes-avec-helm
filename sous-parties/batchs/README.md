# Les batchs et Kubernetes

Pour les traitements batchs dans Kubernetes, un concept existe : Les jobs.


## Job : La notion de batch dans Kubernetes

Le principe des Job est la définition d'un traitement que l'on désire voir réalisé. 

Pour l'exemple nous allons utiliser l'application "Hello world", hébergée ici : 

- https://github.com/Ragatzino/hello-world-java

Une image docker associée a ce dépot git est hébergée ici : [ragatzino/hello-world](https://hub.docker.com/repository/docker/ragatzino/helloworld)

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: helloworldjob
spec:
  ttlSecondsAfterFinished: 10
  template:
    spec:
      containers:
      - name: helloworld
        image: ragatzino/helloworld
        ## L'image helloworld est un jdk avec un jar disponible à ./application.jar
        command: ["java",  "-jar", "application.jar"]
      restartPolicy: Never
```

> hello-world-job.yaml

On peut imaginer l'équivalent avec une application java avec batch. La différence serait dans la connexion au service "base de données" dans la configuration de l'environnement. 

## CronJob: Répeter les batchs sur une periode

De la même manière que l'on peut lancer des traitements de manière ponctuelle, on peut rajouter une dimension repétée a ces traitements.

Exemple : Récupération de tweets toutes les heures sur un sujet 

=> https://github.com/Ragatzino/fetch-tweets python qui fetch sur l'api twitter


## [Retour a la page principale ↩️ ](../../README.md)